﻿using System.ComponentModel.DataAnnotations;

namespace Donation.Models
{
    public class CustomerResponse
    {
        [Required (AllowEmptyStrings = false, ErrorMessage = "Укажите имя")]
        
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Укажите Ваш почтовый адрес")]
        public string Address { get; set; }
       
        [Required(ErrorMessage = "Укажите адрес электронной почты")]
        [EmailAddress(ErrorMessage = "Неверный адрес электронной почты")]
        public string Email { get; set; }

        public string Comment { get; set; }
        
         [Required(ErrorMessage = "Необходимо указать способ оплаты")]
        public string Payment { get; set; }

        [Required(ErrorMessage = "Необходимо ввести сумму")]
        public string Sum { get; set; }
        

    }
}

