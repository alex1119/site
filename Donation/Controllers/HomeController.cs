﻿using System.Reflection.Metadata.Ecma335;
using Donation.Core;
using Donation.Models;
using Microsoft.AspNetCore.Mvc;

namespace Donation.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet] // перезагрузка/переход
        public ViewResult DonationForm()
        {
            return View();
        }
        [HttpPost] // отправляет данные
        public ViewResult DonationForm([FromForm] CustomerResponse response)
        {
            if (ModelState.IsValid)
            { 
                Repository.AddResponse(response);  
                return View("Thanks", response);
            }

            return View();
        }

        [HttpGet]
        public ViewResult ListOfPayers()
        {
            return View(Repository.Responses);
        }
      
    }
}
