﻿using System.Collections.Generic;
using Donation.Models;

namespace Donation.Core
{
    public static class Repository
    {
        private static List<CustomerResponse> _responses = new List<CustomerResponse>();

        public static List<CustomerResponse> Responses => _responses;

        public static void AddResponse(CustomerResponse response)
        {
            _responses.Add(response);
        }
    }
}
